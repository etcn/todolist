import datetime
import logging
import json

TIME_REPEATER = ['hour', 'minute', 'second']
TIME_MODEL = '%Y/%m/%d %H:%M'


def string_to_datetime(string_):
    return datetime.datetime.strptime(string_, TIME_MODEL)


def datetime_to_string(datetime_):
    return datetime_.strftime(TIME_MODEL)


class WidgetEdit:

    def __init__(self):
        pass

    def add_edit_action(self):
        pass

    def edit_widget(self, dict_):
        for attr_name, attr_data in dict_.items():
            setattr(self, attr_name, attr_data)

        self.add_edit_action()

    def to_dict(self):
        return {self.__class__.__name__: vars(self)}


class Date(WidgetEdit):

    def __init__(self):
        WidgetEdit.__init__(self)
        self.time_ = None

    def __repr__(self):
        return '<{} widget: time {}>'.format(self.__class__.__name__, self.time_)


class TextNotes(WidgetEdit):

    def __init__(self):
        WidgetEdit.__init__(self)
        self.notes = None

    def __repr__(self):
        return '<{} widget: contain {}>'.format(self.__class__.__name__, self.notes)


class ListItem(WidgetEdit):

    def __init__(self):
        WidgetEdit.__init__(self)
        self.state = False
        self.note = ''

    def __repr__(self):
        return '<{}: state {} note {}>'.format(self.__class__.__name__, self.state, self.note)


class ListNotes(WidgetEdit):

    def __init__(self):
        WidgetEdit.__init__(self)
        self.list_notes = list()

    def new_item(self, item_data):
        new_item = ListItem()
        new_item.edit_widget(item_data)

        self.list_notes.append(new_item)

    def remove_item(self, item):
        self.list_notes.pop(item)

    def clear_list(self):
        self.list_notes.clear()

    def to_dict(self):
        return {self.__class__.__name__: [vars(note) for note in self.list_notes]}

    def edit_widget(self, dict_):
        for item_data in dict_:
            self.new_item(item_data)

    def __repr__(self):
        return '<{} widget: contain {}>'.format(self.__class__.__name__, self.list_notes)


class Reminder(WidgetEdit):

    def __init__(self):
        WidgetEdit.__init__(self)
        self.time_reference = None
        self.value = None
        self.frequency = None
        self.time_to_alert = None

    def add_edit_action(self):
        datetime_time_reference = string_to_datetime(self.time_reference)
        if datetime_time_reference is None or not self.value or not self.frequency:
            return

        offset_datetime = eval('datetime.timedelta({frequency}s={value})'.format(
            frequency=self.frequency,
            value=self.value
        ))

        self.time_to_alert = datetime_to_string(datetime_time_reference - offset_datetime)

    def is_time(self, current_time):
        datetime_time_alert = string_to_datetime(self.time_to_alert)

        if datetime_time_alert <= current_time <= datetime_time_alert + datetime.timedelta(minutes=5):
            return self.time_reference

        return False

    def __repr__(self):
        return '<{} widget: at time {}>'.format(self.__class__.__name__, self.time_reference)


class Repeater(WidgetEdit):

    def __init__(self):
        WidgetEdit.__init__(self)
        self.time_reference = None
        self.frequency = None
        self.next_time_reference = None

    def update_next_time_reference(self):
        datetime_time_reference = string_to_datetime(self.time_reference)
        if datetime_time_reference is None or not self.frequency:
            return

        if self.frequency == 'month':
            month_value = datetime_time_reference.month + 1
            if month_value <= 12:
                datetime_next_time_reference = datetime_time_reference.replace(month=month_value)
            else:
                datetime_next_time_reference = datetime_time_reference.replace(month=1, year=(datetime_time_reference.year+1))
        elif self.frequency == 'year':
            datetime_next_time_reference = datetime_time_reference.replace(year=(datetime_time_reference.year+1))
        else:
            datetime_next_time_reference = datetime_time_reference + eval(
                'datetime.timedelta({}s=1)'.format(self.frequency)
            )

        self.next_time_reference = datetime_to_string(datetime_next_time_reference)

    def add_edit_action(self):
        self.update_next_time_reference()

    def is_time(self, current_time):
        datetime_next_time_reference = string_to_datetime(self.next_time_reference)
        if self.frequency in TIME_REPEATER and current_time.time() != datetime_next_time_reference.time():
            return False

        if self.frequency not in TIME_REPEATER \
                and current_time.date() != datetime_next_time_reference.date() \
                or current_time.time() != datetime_next_time_reference.time():
            return False

        self.update_next_time_reference()

        return self.time_reference

    def __repr__(self):
        return '<{} widget: Time reference {}, Frequency {}, Current time reference {}>'.format(
            self.__class__.__name__,
            self.time_reference,
            self.frequency,
            self.next_time_reference
        )


class Task:

    def __init__(self):
        self.name = 'New task'
        self.list_widgets = list()

    def add_widget(self, widget_):
        self.list_widgets.append(widget_)

    def to_dict(self):
        widgets_dict = dict()
        for widget in self.list_widgets:
            widgets_dict = {**widgets_dict, **widget.to_dict()}

        return {self.name: widgets_dict}

    def from_dict(self, dict_):
        self.list_widgets.clear()

        for widget_type, widget_data in dict_.items():
            if widget_data is None:
                continue

            new_widget = eval(widget_type)()
            new_widget.edit_widget(widget_data)

            self.add_widget(new_widget)

    def __repr__(self):
        return '<Task: {}, List Widgets: {}>'.format(self.name, self.list_widgets)


class AllTasks:

    def __init__(self):
        self.list_tasks = list()

    def new_task(self):
        new_task = Task()
        self.list_tasks.append(new_task)

        return new_task

    @staticmethod
    def edit_task(task, task_name, data):
        task.name = task_name
        task.from_dict(data)

    def delete_task(self, task):
        self.list_tasks.remove(task)

    def is_task_alert(self, time_):
        task_alert = dict()
        for task in self.list_tasks:
            for widget in task.list_widgets:
                if not isinstance(widget, Reminder) and not isinstance(widget, Repeater):
                    continue

                is_time = widget.is_time(time_)
                if is_time:
                    task_alert[task] = is_time

        return task_alert

    def to_json(self, json_path):
        tasks_dict = dict()
        for task in self.list_tasks:
            tasks_dict.update(task.to_dict())

        with open(json_path, 'w') as json_file:
            json.dump(tasks_dict, json_file, indent=4)

        logging.info('Tasks saved to {}'.format(json_path))

    def from_json(self, json_path):
        with open(json_path, 'r') as json_file:
            loaded_tasks = json.load(json_file)

        for task_name, task_data in loaded_tasks.items():
            new_task = Task()
            new_task.name = task_name

            new_task.from_dict(task_data)

            self.list_tasks.append(new_task)

        return self.list_tasks

    def __repr__(self):
        return '<All tasks: {}>'.format(self.list_tasks)
