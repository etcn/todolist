# Tasks Manager



**Just start app from __main__.py**
> Python version used : 3.7


TASK DEFINITION:
- task_name
- task_widgets

TYPES OF WIDGETS AVAILABLE :
- date
- repeater
- reminder
- text notes
- list notes

FUTURE WIDGETS :
- quick launch .exe/url
- API call (meteo, traffic, transport, music)
