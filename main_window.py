# Icon made by Pixel perfect from www.flaticon.com
from PySide2 import QtWidgets, QtCore, QtGui, QtMultimedia

# reorder tasks
# CONSTANTS file

# WIDGETS TO ADD
# ['resources/icons/launch.png', 'resources/icons/meteo.png', 'resources/icons/pin.png', 'resources/icons/spotify.png']
# AVAILABLE WIDGETS
WIDGETS = [('Date', 'resources/icons/date.png'),
           ('Repeater', 'resources/icons/repeater.png'),
           ('Reminder', 'resources/icons/reminder.png'),
           ('TextNotes', 'resources/icons/textnotes.png'),
           ('ListNotes', 'resources/icons/listnotes.png')]
LIST_WIDGETS_ICON_SIZE = 50
HEADER_ELEMENTS_SIZE = 30
QDATETIME_FORMAT = "yyyy/MM/dd hh:mm"


def _get_widget_from_task(widget_name, task):
    for widget in task.list_widgets:
        if widget.__class__.__name__ == widget_name:
            return widget

    return None


# CUSTOMS BUTTONS & ICONS
class CustomButton(QtWidgets.QPushButton):

    def __init__(self, icon_path, parent=None):
        super().__init__(parent)
        self.setFixedSize(HEADER_ELEMENTS_SIZE, HEADER_ELEMENTS_SIZE)
        self.setFlat(True)
        self.pixmap_on = QtGui.QPixmap(icon_path)
        self.pixmap_off = QtGui.QPixmap(icon_path)
        self.pixmap_off.fill((QtGui.QColor('black')))
        self.pixmap_off.setMask(self.pixmap_on.createHeuristicMask())
        self.setIcon(self.pixmap_on)
        self.setIconSize(QtCore.QSize(self.width()-2, self.height()-2))

    def toggle_on(self):
        self.setIcon(self.pixmap_on)

    def toggle_off(self):
        self.setIcon(self.pixmap_off)


class ListWidgetsIcon(QtGui.QIcon):

    def __init__(self, widget_icon_path, parent=None):
        super().__init__(parent)
        self.pixmap_on = QtGui.QPixmap(widget_icon_path)
        self.pixmap_off = QtGui.QPixmap(widget_icon_path)
        self.mask = self.pixmap_on.createMaskFromColor(QtGui.QColor('black'), QtCore.Qt.MaskOutColor)
        self.pixmap_off.fill((QtGui.QColor('grey')))
        self.pixmap_off.setMask(self.mask)
        self.addPixmap(self.pixmap_on, QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.addPixmap(self.pixmap_off, QtGui.QIcon.Normal, QtGui.QIcon.Off)


class ToggleWidget(QtWidgets.QPushButton):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setFixedSize(30, 30)
        self.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.setCheckable(True)
        self.setAccessibleName('ToggleWidget')


# LIST ITEMS (TASK & WIDGETS)
class NewTaskItem(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setup_widgets()
        self.setup_layouts()
        self.add_widgets_to_layout()

    def setup_widgets(self):
        self.wi_task_marker = QtWidgets.QWidget()
        self.lbl_task_name = QtWidgets.QLabel('Task_Name')
        self.lbl_task_date = QtWidgets.QLabel()
        self.lbl_task_time_remaining = QtWidgets.QLabel()
        self.frm_task_line_sep = QtWidgets.QFrame()
        self.lw_task_widgets = QtWidgets.QListWidget()

        self.setFixedHeight(80)

        self.wi_task_marker.setStyleSheet("QWidget {background-color: #B3D7E8;}")
        self.wi_task_marker.setMinimumSize(12, 0)
        self.wi_task_marker.setMaximumSize(12, 80)

        self.lbl_task_name.setStyleSheet("QLabel {font-size: 18px; font-family: Arial Black; font-weight: bold;}")
        self.lbl_task_name.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)
        self.lbl_task_name.setAttribute(QtCore.Qt.WA_NoSystemBackground)
        self.lbl_task_name.setMinimumHeight(40)

        self.lbl_task_date.setStyleSheet("QLabel {color: #B6BEBF;}")
        self.lbl_task_date.setAttribute(QtCore.Qt.WA_NoSystemBackground)

        self.lbl_task_time_remaining.setStyleSheet("QLabel {font-size: 10px; font-style: italic; color: #B6BEBF;}")
        self.lbl_task_time_remaining.setAttribute(QtCore.Qt.WA_NoSystemBackground)

        self.frm_task_line_sep.setFrameShape(QtWidgets.QFrame.VLine)
        self.frm_task_line_sep.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frm_task_line_sep.setAttribute(QtCore.Qt.WA_NoSystemBackground)

        self.lw_task_widgets.setMinimumSize(QtCore.QSize(0, 80))
        self.lw_task_widgets.setMaximumSize(QtCore.QSize(50, 80))
        self.lw_task_widgets.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Fixed)
        self.lw_task_widgets.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.lw_task_widgets.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.lw_task_widgets.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.lw_task_widgets.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.lw_task_widgets.setMovement(QtWidgets.QListView.Static)
        self.lw_task_widgets.setFlow(QtWidgets.QListView.TopToBottom)
        self.lw_task_widgets.setViewMode(QtWidgets.QListView.IconMode)
        self.lw_task_widgets.setIconSize(QtCore.QSize(18, 18))
        self.lw_task_widgets.setUniformItemSizes(True)
        self.lw_task_widgets.viewport().setAutoFillBackground(False)
        self.lw_task_widgets.setAccessibleName('lw_task_widgets')
        self.lw_task_widgets.setAttribute(QtCore.Qt.WA_NoSystemBackground)

    def setup_layouts(self):
        self.new_task_layout = QtWidgets.QHBoxLayout()
        self.new_task_layout.setContentsMargins(0, 0, 0, 0)

        self.new_task_infos_layout = QtWidgets.QVBoxLayout()
        self.new_task_infos_layout.setContentsMargins(2, 2, 2, 2)
        self.new_task_infos_layout.setSpacing(0)

    def add_widgets_to_layout(self):
        self.setLayout(self.new_task_layout)

        self.new_task_layout.addWidget(self.wi_task_marker)
        self.new_task_layout.addLayout(self.new_task_infos_layout)
        self.new_task_layout.addWidget(self.frm_task_line_sep)
        self.new_task_layout.addWidget(self.lw_task_widgets)

        self.new_task_infos_layout.addWidget(self.lbl_task_name)
        self.new_task_infos_layout.addWidget(self.lbl_task_date)
        self.new_task_infos_layout.addWidget(self.lbl_task_time_remaining)

    def sizeHint(self):
        return QtCore.QSize(200, 80)

    def set_task_infos(self, task_data):
        self.wi_task_marker.setStyleSheet("QWidget {background-color: #B3D7E8}")
        self.lbl_task_name.setText(task_data.name)

        date_widget = _get_widget_from_task('Date', task_data)
        if date_widget:
            self.lbl_task_date.setText(date_widget.time_)

            task_time_left = QtCore.QDateTime.currentDateTime().secsTo(
                            QtCore.QDateTime.fromString(date_widget.time_, QDATETIME_FORMAT))
            start, end = ('in', '') if task_time_left >= 0 else ('was', 'ago')
            if task_time_left <= 0:
                self.wi_task_marker.setStyleSheet("QWidget {background-color: #CC9991}")

            if abs(task_time_left) >= 86400:
                self.lbl_task_time_remaining.setText(f'{start} {round(abs(task_time_left)/86400, 1)} day(s) {end}')
            elif abs(task_time_left) > 3600:
                self.lbl_task_time_remaining.setText(f'{start} {round(abs(task_time_left)/3600, 1)} hour(s) {end}')
            else:
                self.lbl_task_time_remaining.setText(f'{start} {round(abs(task_time_left)/60, 1)} minute(s) {end}')

        for widget in WIDGETS:
            if _get_widget_from_task(widget[0], task_data):
                item = QtWidgets.QListWidgetItem()
                icon = ListWidgetsIcon(widget[1])
                icon = icon.pixmap_on
                item.setIcon(icon)
                item.setFlags(QtCore.Qt.ItemIsEnabled)
                self.lw_task_widgets.addItem(item)


class BaseWidgetEditor(QtWidgets.QWidget):

    def __init__(self, widget_name='', widget_icon='', btn_save=None, parent=None):
        super().__init__(parent)
        self.wi_height = 40
        self.btn_save = btn_save
        self.widget_item = widget_icon
        if self.widget_item:
            self.widget_icon = self.widget_item.data(0)

        self.widget_editor_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.widget_editor_layout)
        self.widget_editor_layout.setDirection(QtWidgets.QBoxLayout.TopToBottom)
        self.widget_editor_layout.setSpacing(20)
        self.widget_editor_layout.setContentsMargins(20, 20, 20, 20)
        self.widget_editor_layout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)

        self.widget_editor_header_layout = QtWidgets.QHBoxLayout()
        self.widget_editor_header_layout.setContentsMargins(0, 0, 0, 0)
        self.widget_editor_header_layout.setSpacing(0)

        self.cb_widget = ToggleWidget()
        self.cb_widget.hide()

        self.lbl_widget_title = QtWidgets.QLabel()
        self.lbl_widget_title.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        self.lbl_widget_title.setFixedHeight(self.wi_height)
        self.lbl_widget_title.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_widget_title.setText(widget_name)
        self.lbl_widget_title.setAccessibleName('lbl_widget_title')
        self.lbl_widget_title.setAttribute(QtCore.Qt.WA_NoSystemBackground)

        self.w_balance = QtWidgets.QSpacerItem(40, 30, QtWidgets.QSizePolicy.Fixed,  QtWidgets.QSizePolicy.Minimum)

        self.dte_date = QtWidgets.QDateTimeEdit()
        self.dte_date.setMinimumHeight(self.wi_height)
        self.dte_date.setCalendarPopup(True)
        self.dte_date.setMinimumDateTime(QtCore.QDateTime.currentDateTime())
        self.dte_date.setDateTime(QtCore.QDateTime.currentDateTime().addSecs(3600))
        self.dte_date.setDisplayFormat(QDATETIME_FORMAT)
        self.dte_date.setAttribute(QtCore.Qt.WA_NoSystemBackground)
        self.dte_date.hide()

        self.widget_editor_layout.addLayout(self.widget_editor_header_layout)
        self.widget_editor_header_layout.addWidget(self.cb_widget)
        self.widget_editor_header_layout.addWidget(self.lbl_widget_title)
        self.widget_editor_header_layout.addItem(self.w_balance)
        self.widget_editor_layout.addWidget(self.dte_date)

        self.dte_date.dateTimeChanged.connect(self.data_to_save)
        self.cb_widget.toggled.connect(self.toggle_widget_icon)

    def toggle_widget_icon(self):
        self.data_to_save()
        if self.cb_widget.isChecked():
            self.widget_item.setIcon(self.widget_icon.pixmap_on)
        else:
            self.widget_item.setIcon(self.widget_icon.pixmap_off)

    def data_to_save(self):
        self.btn_save.toggle_on()
        self.btn_save.setEnabled(True)


class DateWidgetEditor(BaseWidgetEditor):

    def __init__(self, widget_name, widget_icon, btn_save, parent=None):
        super().__init__(widget_name, widget_icon, btn_save, parent)
        self.btn_save = btn_save
        self.cb_widget.show()
        self.dte_date.show()

    def set_date(self, date_class=None):
        self.cb_widget.setChecked(False)
        if date_class:
            self.cb_widget.setChecked(True)
            self.dte_date.setDateTime(QtCore.QDateTime.fromString(date_class.time_, QDATETIME_FORMAT))
        else:
            self.dte_date.setDateTime(QtCore.QDateTime.currentDateTime().addSecs(3600))

    def get_date(self):
        get_date = {'time_': self.dte_date.text()} if self.cb_widget.isChecked() else None
        return get_date


class RepeaterWidgetEditor(BaseWidgetEditor):

    def __init__(self, widget_name, widget_icon, btn_save, parent=None):
        super().__init__(widget_name, widget_icon, btn_save, parent)
        self.btn_save = btn_save
        self.cb_widget.show()
        self.dte_date.show()

        repeat_layout = QtWidgets.QHBoxLayout()
        lbl_every = QtWidgets.QLabel("every ")
        lbl_every.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        lbl_every.setFixedHeight(self.wi_height)
        lbl_every.setAttribute(QtCore.Qt.WA_NoSystemBackground)
        self.cbb_repeater = QtWidgets.QComboBox()
        self.cbb_repeater.setMinimumHeight(self.wi_height)
        self.cbb_repeater.setAttribute(QtCore.Qt.WA_NoSystemBackground)
        self.cbb_repeater.addItems(['hour', 'day', 'week', 'month', 'year'])

        self.widget_editor_layout.addWidget(self.dte_date)
        self.widget_editor_layout.addLayout(repeat_layout)
        repeat_layout.addWidget(lbl_every)
        repeat_layout.addWidget(self.cbb_repeater)

        self.dte_date.dateTimeChanged.connect(super().data_to_save)
        self.cbb_repeater.currentIndexChanged.connect(super().data_to_save)

    def set_repeater(self, repeater_class=None):
        self.cb_widget.setChecked(False)
        if repeater_class:
            self.cb_widget.setChecked(True)
            time_reference = QtCore.QDateTime.fromString(repeater_class.time_reference, QDATETIME_FORMAT)
            self.dte_date.setDateTime(time_reference)
            self.cbb_repeater.setCurrentText(repeater_class.frequency)
        else:
            self.dte_date.setDateTime(QtCore.QDateTime.currentDateTime().addSecs(3600))
            self.cbb_repeater.setCurrentText('day')

    def get_repeater(self):
        get_repeater = None
        if self.cb_widget.isChecked():
            get_repeater = {
                'time_reference': self.dte_date.text(),
                'frequency': self.cbb_repeater.currentText()
            }
        return get_repeater


class ReminderWidgetEditor(BaseWidgetEditor):

    def __init__(self, widget_name, widget_icon, btn_save, parent=None):
        super().__init__(widget_name, widget_icon, btn_save, parent)
        self.btn_save = btn_save
        self.cb_widget.show()
        self.dte_date.show()

        reminder_layout = QtWidgets.QHBoxLayout()
        self.spnb_reminder = QtWidgets.QSpinBox()
        self.spnb_reminder.setMinimumHeight(self.wi_height)
        self.spnb_reminder.setAttribute(QtCore.Qt.WA_NoSystemBackground)
        self.cbb_reminder = QtWidgets.QComboBox()
        self.cbb_reminder.setMinimumHeight(self.wi_height)
        self.cbb_reminder.setAttribute(QtCore.Qt.WA_NoSystemBackground)
        self.cbb_reminder.addItem('minute(s)', userData='minute')
        self.cbb_reminder.addItem('hour(s)', userData='hour')
        self.cbb_reminder.addItem('day(s)', userData='day')
        lbl_reminder = QtWidgets.QLabel(" before ")
        lbl_reminder.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        lbl_reminder.setFixedHeight(self.wi_height)

        self.widget_editor_layout.addWidget(self.dte_date)
        self.widget_editor_layout.addLayout(reminder_layout)
        reminder_layout.addWidget(self.spnb_reminder)
        reminder_layout.addWidget(self.cbb_reminder)
        reminder_layout.addWidget(lbl_reminder)

        self.dte_date.dateTimeChanged.connect(super().data_to_save)
        self.spnb_reminder.valueChanged.connect(super().data_to_save)
        self.cbb_reminder.currentIndexChanged.connect(super().data_to_save)

    def set_reminder(self, reminder_class=None):
        self.cb_widget.setChecked(False)
        if reminder_class:
            self.cb_widget.setChecked(True)
            time_reference = QtCore.QDateTime.fromString(reminder_class.time_reference, QDATETIME_FORMAT)
            self.dte_date.setDateTime(time_reference)
            self.spnb_reminder.setValue(int(reminder_class.value))
            self.cbb_reminder.setCurrentIndex(self.cbb_reminder.findData(reminder_class.frequency))
        else:
            self.dte_date.setDateTime(QtCore.QDateTime.currentDateTime().addSecs(3600))
            self.spnb_reminder.setValue(15)
            self.cbb_reminder.setCurrentIndex(self.cbb_reminder.findData('minute'))

    def get_reminder(self):
        get_reminder = None
        if self.cb_widget.isChecked():
            get_reminder = {
                'time_reference': self.dte_date.text(),
                'frequency': self.cbb_reminder.currentData(),
                'value': self.spnb_reminder.text()
            }
        return get_reminder


class TextNotesWidgetEditor(BaseWidgetEditor):

    def __init__(self, widget_name, widget_icon, btn_save, parent=None):
        super().__init__(widget_name, widget_icon, btn_save, parent)
        self.btn_save = btn_save
        self.cb_widget.show()

        self.te_textnotes = QtWidgets.QTextEdit()
        self.widget_editor_layout.addWidget(self.te_textnotes)

        self.te_textnotes.textChanged.connect(super().data_to_save)

    def set_textnotes(self, notes_class=None):
        self.cb_widget.setChecked(False)
        if notes_class:
            self.cb_widget.setChecked(True)
            self.te_textnotes.setPlainText(notes_class.notes)
        else:
            self.te_textnotes.setPlainText('')

    def get_textnotes(self):
        get_textnotes = None
        if self.cb_widget.isChecked():
            get_textnotes = {'notes': self.te_textnotes.toPlainText()}
        return get_textnotes


class ListNotesWidgetEditor(BaseWidgetEditor):

    def __init__(self, widget_name, widget_icon, btn_save, parent=None):
        super().__init__(widget_name, widget_icon, btn_save, parent)
        self.btn_save = btn_save
        self.cb_widget.show()

        listnotes_layout = QtWidgets.QGridLayout()
        btn_listnotes_add = QtWidgets.QPushButton('+')
        btn_listnotes_add.setAccessibleName('btn_listnotes')
        btn_listnotes_add.setFlat(True)
        btn_listnotes_add.setMinimumSize(30, 30)
        btn_listnotes_del = QtWidgets.QPushButton('-')
        btn_listnotes_del.setAccessibleName('btn_listnotes')
        btn_listnotes_del.setFlat(True)
        btn_listnotes_del.setMinimumSize(30, 30)

        btn_sizepolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        btn_sizepolicy.setHorizontalStretch(20)
        btn_sizepolicy.setVerticalStretch(0)
        btn_sizepolicy.setHeightForWidth(btn_listnotes_add.sizePolicy().hasHeightForWidth())
        btn_listnotes_add.setSizePolicy(btn_sizepolicy)
        btn_listnotes_del.setSizePolicy(btn_sizepolicy)

        self.lw_listnotes = QtWidgets.QListWidget()
        self.lw_listnotes.setAccessibleName('lw_listnotes')
        self.lw_listnotes.setAlternatingRowColors(True)
        self.lw_listnotes.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.lw_listnotes.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

        lw_listnotes_sizepolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        lw_listnotes_sizepolicy.setHorizontalStretch(80)
        lw_listnotes_sizepolicy.setVerticalStretch(0)
        lw_listnotes_sizepolicy.setHeightForWidth(btn_listnotes_add.sizePolicy().hasHeightForWidth())
        self.lw_listnotes.setSizePolicy(lw_listnotes_sizepolicy)

        listnotes_layout.addWidget(self.lw_listnotes, 0, 0, 2, 1)
        listnotes_layout.addWidget(btn_listnotes_add, 0, 1, 1, 1)
        listnotes_layout.addWidget(btn_listnotes_del, 1, 1, 1, 1)

        self.widget_editor_layout.addLayout(listnotes_layout)

        btn_listnotes_add.clicked.connect(self.add_listnotes)
        btn_listnotes_del.clicked.connect(self.del_listnotes)

        self.lw_listnotes.itemChanged.connect(super().data_to_save)

    def add_listnotes(self):
        new_note_item = QtWidgets.QListWidgetItem()
        new_note_item.setCheckState(QtCore.Qt.Unchecked)
        new_note_item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
        self.lw_listnotes.addItem(new_note_item)

    def del_listnotes(self):
        listnotes_selection = self.lw_listnotes.selectedItems()
        if listnotes_selection:
            for note in listnotes_selection:
                self.lw_listnotes.takeItem(self.lw_listnotes.indexFromItem(note).row())

    def set_listnotes(self, listnotes_class=None):
        self.cb_widget.setChecked(False)
        self.lw_listnotes.clear()
        if listnotes_class:
            self.cb_widget.setChecked(True)
            for item in listnotes_class.list_notes:
                new_item = QtWidgets.QListWidgetItem(item.note)
                new_item.setCheckState(QtCore.Qt.Checked if item.state is True else QtCore.Qt.Unchecked)
                self.lw_listnotes.addItem(new_item)

    def get_listnotes(self):
        if self.cb_widget.isChecked():
            get_list_notes = []
            for note_item in range(self.lw_listnotes.count()):
                note_state = True if self.lw_listnotes.item(note_item).checkState() == QtCore.Qt.Checked else False
                note_text = self.lw_listnotes.item(note_item).text()
                note_dict = dict()
                note_dict['state'] = note_state
                note_dict['note'] = note_text
                get_list_notes.append(note_dict)
        else:
            get_list_notes = None
        return get_list_notes


class AlertWindow:
    def __init__(self, title, message):
        QtMultimedia.QSound.play("resources/tone.wav")
        alert_window = QtWidgets.QMessageBox()
        flags = QtCore.Qt.Dialog | QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint
        alert_window.setWindowFlags(flags)
        alert_window.setWindowTitle(title)
        alert_window.resize(400, 200)
        alert_window.setText(message)
        alert_window.exec_()


# MainWindow
class MainWindow(QtWidgets.QWidget):

    def __init__(self, screen_size, application=None, parent=None):
        super().__init__(parent)
        self.application = application
        self.setWindowTitle('Task Manager')
        self.resize(screen_size.width()/2.5, screen_size.height()/2.5)
        self.setup_ui()

        self._timer = QtCore.QTimer()
        self._timer.timeout.connect(self.application.is_task_alert)
        self._timer.start(1000)

    def is_task_alert(self, tasks_alert=None):
        if tasks_alert:
            for task, time_ in tasks_alert.items():
                for task_index in range(self.lw_tasks.count()):
                    task_item = self.lw_tasks.item(task_index)
                    if task == task_item.data(QtCore.Qt.UserRole) and not task_item.data(QtCore.Qt.CheckStateRole):
                        task_item.setData(QtCore.Qt.CheckStateRole, True)
                        message = '{0}\n{1}'.format(task.name, time_)
                        AlertWindow('Reminder', message)

    def setup_ui(self):
        self.create_widgets()
        self.modify_widgets()
        self.create_layouts()
        self.modify_layouts()
        self.add_widgets_to_layout()
        self.setup_connections()

    def create_widgets(self):
        self.task_widget_splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.wi_tasks_list = QtWidgets.QWidget()
        self.wi_task_edit = QtWidgets.QWidget()

        self.btn_add = CustomButton('resources/icons/add.png')
        self.btn_del = CustomButton('resources/icons/del.png')
        self.lw_tasks = QtWidgets.QListWidget()

        self.widget_editor_splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.le_task_name = QtWidgets.QLineEdit()
        self.btn_save = CustomButton('resources/icons/save.png')
        self.lw_widgets = QtWidgets.QListWidget()
        self.wi_widget_editor = QtWidgets.QWidget()

        widgets_iterator = iter([widget[1] for widget in WIDGETS])
        self.widget_blank = BaseWidgetEditor()
        widget_icon_time = self.add_widget(next(widgets_iterator))
        self.widget_date = DateWidgetEditor('Date', widget_icon_time, self.btn_save)
        widget_icon_repeat = self.add_widget(next(widgets_iterator))
        self.widget_repeater = RepeaterWidgetEditor('Repeater', widget_icon_repeat, self.btn_save)
        widget_icon_reminder = self.add_widget(next(widgets_iterator))
        self.widget_reminder = ReminderWidgetEditor('Reminder', widget_icon_reminder, self.btn_save)
        widget_icon_notes = self.add_widget(next(widgets_iterator))
        self.widget_textnotes = TextNotesWidgetEditor('TextNotes', widget_icon_notes, self.btn_save)
        widget_icon_list_notes = self.add_widget(next(widgets_iterator))
        self.widget_listnotes = ListNotesWidgetEditor('ListNotes', widget_icon_list_notes, self.btn_save)

    def modify_widgets(self):
        css_file = 'resources/style.css'
        with open(css_file, 'r') as f:
            self.setStyleSheet(f.read())

        self.task_widget_splitter.setHandleWidth(6)
        self.task_widget_splitter.setMaximumHeight(50)

        self.lw_tasks.setAccessibleName('lw_tasks')
        self.lw_tasks.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.lw_tasks.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.lw_tasks.setMinimumWidth(300)
        self.lw_tasks.setGridSize(QtCore.QSize(200, 84))

        self.widget_editor_splitter.setHandleWidth(6)

        self.le_task_name.setAccessibleName('le_task_name')
        self.le_task_name.setAlignment(QtCore.Qt.AlignCenter)
        self.le_task_name.setMinimumHeight(HEADER_ELEMENTS_SIZE)
        self.le_task_name.setFrame(False)
        self.le_task_name.setPlaceholderText("Task_Name")

        self.btn_save.toggle_off()
        self.btn_save.setEnabled(False)

        self.lw_widgets.setEnabled(False)
        self.lw_widgets.setAccessibleName('lw_widgets')
        self.lw_widgets.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.lw_widgets.setViewMode(QtWidgets.QListView.IconMode)
        self.lw_widgets.setMovement(QtWidgets.QListView.Static)
        self.lw_widgets.setIconSize(QtCore.QSize(LIST_WIDGETS_ICON_SIZE, LIST_WIDGETS_ICON_SIZE))
        self.lw_widgets.setFlow(QtWidgets.QListView.LeftToRight)
        self.lw_widgets.setResizeMode(QtWidgets.QListView.Adjust)
        self.lw_widgets.setUniformItemSizes(True)

        self.wi_widget_editor.setMinimumHeight(180)

    def create_layouts(self):
        self.main_layout = QtWidgets.QHBoxLayout()

        self.tasks_list_layout = QtWidgets.QVBoxLayout()
        self.tasks_list_btn_layout = QtWidgets.QHBoxLayout()

        self.task_edit_layout = QtWidgets.QVBoxLayout()
        self.task_edit_header_layout = QtWidgets.QHBoxLayout()
        self.widget_editor_layout = QtWidgets.QStackedLayout()

    def modify_layouts(self):
        self.tasks_list_btn_layout.setAlignment(QtCore.Qt.AlignLeft)

    def add_widgets_to_layout(self):
        self.setLayout(self.main_layout)

        self.main_layout.addWidget(self.task_widget_splitter)

        self.wi_tasks_list.setLayout(self.tasks_list_layout)
        self.task_widget_splitter.addWidget(self.wi_tasks_list)
        self.wi_task_edit.setLayout(self.task_edit_layout)
        self.task_widget_splitter.addWidget(self.wi_task_edit)

        self.tasks_list_layout.addLayout(self.tasks_list_btn_layout)
        self.tasks_list_btn_layout.addWidget(self.btn_add)
        self.tasks_list_btn_layout.addWidget(self.btn_del)
        self.tasks_list_layout.addWidget(self.lw_tasks)

        self.task_edit_layout.addLayout(self.task_edit_header_layout)
        self.task_edit_header_layout.addWidget(self.le_task_name)
        self.task_edit_header_layout.addWidget(self.btn_save)

        self.task_edit_layout.addWidget(self.widget_editor_splitter)

        self.widget_editor_splitter.addWidget(self.lw_widgets)
        self.widget_editor_splitter.addWidget(self.wi_widget_editor)
        self.wi_widget_editor.setLayout(self.widget_editor_layout)

        self.widget_editor_layout.addWidget(self.widget_blank)
        self.widget_editor_layout.addWidget(self.widget_date)
        self.widget_editor_layout.addWidget(self.widget_repeater)
        self.widget_editor_layout.addWidget(self.widget_reminder)
        self.widget_editor_layout.addWidget(self.widget_textnotes)
        self.widget_editor_layout.addWidget(self.widget_listnotes)

    def setup_connections(self):
        self.btn_add.clicked.connect(self.add_task)
        self.btn_del.clicked.connect(self.del_task)
        self.lw_tasks.itemSelectionChanged.connect(self.task_selection)
        QtWidgets.QShortcut(QtGui.QKeySequence('Delete'), self.lw_tasks, self.del_task)

        self.lw_widgets.itemSelectionChanged.connect(self.widget_selection)
        self.btn_save.clicked.connect(self.get_widgets_values)
        self.le_task_name.textChanged.connect(self.data_to_save)

    # TASKS
    def add_task(self, task_data=None):
        new_task = NewTaskItem()
        new_item = QtWidgets.QListWidgetItem('')
        new_item.setSizeHint(new_task.sizeHint())
        self.lw_tasks.addItem(new_item)
        self.lw_tasks.setItemWidget(new_item, new_task)

        if not task_data:
            task_data = self.application.new_task()

        new_task.set_task_infos(task_data)
        new_item.setData(QtCore.Qt.UserRole, task_data)
        new_item.setData(QtCore.Qt.CheckStateRole, False)

    def del_task(self):
        selected_item = self.lw_tasks.selectedItems()
        if selected_item:
            selected_index = self.lw_tasks.indexFromItem(selected_item[0]).row()
            task = self.lw_tasks.takeItem(selected_index)
            self.application.delete_task(task.data(QtCore.Qt.UserRole))

    def reset_tasks_list(self):
        self.lw_tasks.clear()

    def task_selection(self):
        selected_item = self.lw_tasks.selectedItems()
        self.lw_widgets.clearSelection()
        self.reset_widgets_values()
        if selected_item:
            task_data = selected_item[0].data(QtCore.Qt.UserRole)
            self.set_widgets_values(task_data)
            self.lw_widgets.setEnabled(True)
        else:
            self.lw_widgets.setEnabled(False)
            self.widget_editor_layout.setCurrentIndex(0)

        self.btn_save.toggle_off()
        self.btn_save.setEnabled(False)

    def data_to_save(self):
        self.btn_save.toggle_on()
        self.btn_save.setEnabled(True)

    # WIDGETS
    def add_widget(self, widget_icon):
        new_item = QtWidgets.QListWidgetItem()
        icon = ListWidgetsIcon(widget_icon)
        new_item.setIcon(icon)
        new_item.setData(0, icon)
        self.lw_widgets.addItem(new_item)
        return new_item

    def widget_selection(self):
        items = self.lw_widgets.selectedItems()
        if items:
            selection = items[0]
            selection_index = self.lw_widgets.indexFromItem(selection).row() + 1
            self.widget_editor_layout.setCurrentIndex(selection_index)
        else:
            self.widget_editor_layout.setCurrentIndex(0)

    def set_widgets_values(self, task_data):
        self.le_task_name.setText(task_data.name)

        for widget in task_data.list_widgets:
            widget_type = widget.__class__.__name__.lower()
            eval(f"self.widget_{widget_type}.set_{widget_type}(widget)")

    def reset_widgets_values(self):
        self.le_task_name.setText('')
        self.widget_date.set_date()
        self.widget_repeater.set_repeater()
        self.widget_reminder.set_reminder()
        self.widget_textnotes.set_textnotes()
        self.widget_listnotes.set_listnotes()

    def get_widgets_values(self):
        selected_item = self.lw_tasks.selectedItems()[0]
        item_class = selected_item.data(QtCore.Qt.UserRole)
        task_data = {'Date': self.widget_date.get_date(),
                     'Repeater': self.widget_repeater.get_repeater(),
                     'Reminder': self.widget_reminder.get_reminder(),
                     'TextNotes': self.widget_textnotes.get_textnotes(),
                     'ListNotes': self.widget_listnotes.get_listnotes()}
        self.btn_save.toggle_off()
        self.btn_save.setEnabled(False)

        self.application.edit_task(item_class, self.le_task_name.text(), task_data)
        # select back item saved
        for task_index in range(self.lw_tasks.count()):
            task_item = self.lw_tasks.item(task_index)
            if item_class == task_item.data(QtCore.Qt.UserRole):
                self.lw_tasks.setItemSelected(task_item, True)
