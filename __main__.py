import datetime
import logging
import sys
import json

from PySide2 import QtWidgets
from main_window import MainWindow
from widgets_store import AllTasks


JSON_PATH = 'tasks_list.json'


def is_json():
    try:
        with open(JSON_PATH):
            pass
    except IOError:
        with open(JSON_PATH, 'w') as json_file:
            json.dump({}, json_file, indent=4)


class TasksApp:

    def __init__(self, argv):
        self.pyside_application = QtWidgets.QApplication(argv)
        screen_size = self.pyside_application.primaryScreen().size()
        self.main_window = MainWindow(screen_size, application=self)
        self.all_tasks = AllTasks()

    def run(self):
        self.main_window.show()

        is_json()
        loaded_tasks = self.all_tasks.from_json(JSON_PATH)

        for task in loaded_tasks:
            self.main_window.add_task(task)

        return self.pyside_application.exec_()

    def new_task(self):
        new_task = self.all_tasks.new_task()
        self.all_tasks.to_json(JSON_PATH)

        return new_task

    def edit_task(self, task, task_name, data):
        self.all_tasks.edit_task(task, task_name, data)
        self.all_tasks.to_json(JSON_PATH)

        self.main_window.reset_tasks_list()

        for task in self.all_tasks.list_tasks:
            self.main_window.add_task(task)

    def delete_task(self, task):
        self.all_tasks.delete_task(task)
        self.all_tasks.to_json(JSON_PATH)

    def is_task_alert(self):
        time_ = datetime.datetime.now().replace(microsecond=0)
        alert_task = self.all_tasks.is_task_alert(time_)

        if alert_task:
            self.main_window.is_task_alert(alert_task)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    application = TasksApp(sys.argv)
    sys.exit(application.run())
